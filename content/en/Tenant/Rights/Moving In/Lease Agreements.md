--- 
title: "Lease Agreements "
linkTitle: "Tenant Lease Agreements"
type: docs
toc_hide: true
weight: 10
--- 

## Lease Agreements 

Most landlords will have you sign a lease before you move in. A lease is a contract stating what the landlord will do and what you as the renter will have to do. As the law will generally make you follow all the terms of the lease, make sure you clearly understand what you have agreed to do. 

### Pay careful attention to the following items: 

 - How much the rent will be per month.				
 - How much the security deposit will be, if there is one.				
 - What day the rent is due and when it is considered late.				
 - How much is the late fee, if you are late with the payment. 
 - How long the lease runs; month to month, six months, a year.
 - How many days advance notice you have to give if you wish to move.
 - Whether the electric, heat, water and sewer are included in the rent.
 - Whether a refrigerator, stove, air conditioner, or other appliances are provided by the andlord.
 - What you must do to get repairs made.
 - Any specific rules or other charges.

Although most landlords have a lease, there is no requirement that there be anything in writing. If you simply pay rent once a month, then it is called a month to month tenancy and starts again each month. Either you or the landlord can end the tenancy by giving written notice at least 30 days before the next rent payment is due. And, as each month is a new tenancy, the landlord must give the same 30 day notice if he or she wants to raise the rent or make other changes. 

The landlord is required to give a written receipt, upon request from the tenant, whenever the tenant pays rent in the form of cash or a money order. This is true by law, even if it’s not stated in a written lease. You should ALWAYS request a receipt every time you pay rent by cash or a money order.
 							
If you want an accounting of all the charges from the landlord and the payments you’ve made, you should make a written request to the landlord. The landlord is then required to give you a written statement showing all the charges and payments over the entire time of the tenancy, or the past 12 months, whichever is shorter. The landlord must provide this within 10 business days after receiving your request. 
