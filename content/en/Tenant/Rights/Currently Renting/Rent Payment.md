--- 
title: "Rent Payement"
linkTitle: "Rent Payment Information"
type: docs
toc_hide: true
weight: 40
--- 

## What Does My Landlord Have to Show Me? 

The landlord is required to give a written receipt, upon request from the tenant, whenever the tenant pays rent in the form of cash or a money order. This is true by law, even if it’s not stated in a written lease. You should ALWAYS request a receipt every time you pay rent by cash or a money order.
 							
If you want an accounting of all the charges from the landlord and the payments you’ve made, you should make a written request to the landlord. The landlord is then required to give you a written statement showing all the charges and payments over the entire time of the tenancy, or the past 12 months, whichever is shorter. The landlord must provide this within 10 business days after receiving your request. 
