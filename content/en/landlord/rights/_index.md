---
title: "Landlord Information Eviction"
linkTitle: "Landlord Information Eviction"
weight: 3
type: docs
description: >
  What do you need to know as a landlord?
---

 
{{% pageinfo %}}

***Disclaimer for all Landlords:** 
Landlords must carefully follow all the rules and procedures required by Virginia law when evicting a tenant; otherwise, the eviction might not be valid. Although these rules and procedures might seem burdensome to the landlord, they are there for a reason. Evictions often occur very quickly, and the end result is serious: the tenant has lost a place to live. The rules help ensure the eviction is justified and that the tenant has enough time to find a new home.

{{% /pageinfo %}}

# Contents
- [Eviction](#eviction)
    - [Notice with Cause](#notice-with-cause)
    - [Avenues to Achieve](#avenues-to-achieve)
    - [Ways to Achieve](#ways-to-achieve)
- [Notice with Cause](#notice-without-cause)
- [Tenant Defenses](#tenant-defenses)
- [Removal of Tenant](#removal-of-tenant)



# Eviction:

## Notice with Cause

### Most common reasons to have cause to evict a tenant:
        1. Tenant’s failure to pay rent; or 
        2. Violation of the lease or rental agreement
        3. Tenant for committing an illegal act (such as drug use or possession on the premises)

### Avenues to Achieve
> Give the tenant notice
> (If applicable) give the tenant a chance to fix the problem—before terminating the tenancy and filing an eviction lawsuit.

### Ways to Achieve
* **Five-Day Notice to Pay Rent or Quit:** 
    * If [tenants are late paying rent](https://www.nolo.com/legal-encyclopedia/what-to-do-when-your-tenant-is-late-with-the-rent.html), the landlord can give them a [five-day notice to pay rent or quit](https://www.nolo.com/legal-encyclopedia/eviction-notices-nonpayment-rent-virginia.html). 
        * This notice will inform the tenants that they have five days to either pay rent or move out of the rental unit. 
        * If the tenants don’t pay rent within the five-day time frame, then the landlord can go to court and file an eviction lawsuit against the tenant. (Va. Code Ann. § 55.1-1245F (2020)).

* **Thirty-Day Notice to Cure or Quit:** 
    * If the tenant violates the lease or rental agreement and that violation can be remedied (such as by making minor repairs to the rental unit or finding a new home for an unauthorized pet), then the landlord must first give the tenant a 30-day notice to cure or quit. 
        * This notice will inform the tenant that the tenant has 21 days to either remedy the violation or move out of the rental unit. 
        * If the tenant does not remedy the violation or move, then the landlord can file an eviction lawsuit against the tenant at the end of the 30 days. (Va. Code Ann. § 55.1-1245A (2020)). 
 
* **Thirty-Day Unconditional Quit Notice:** 
    * If the tenant violates the lease or rental agreement and the violation cannot be remedied (such as causing major damage to the rental unit), then the landlord can give the tenant a 30-day [unconditional quit notice](https://www.nolo.com/legal-encyclopedia/state-laws-unconditional-quit-terminations.html). 
        * The notice must specify the acts and omissions that constituted the breach. This notice will inform the tenant that the tenant must move at the end of the 30-day period or the landlord will file an eviction lawsuit against the tenant. (Va. Code Ann. § 55.1-1245C (2020)). 
        * Virginia landlords can also serve 30-day unconditional quit notices when tenants have been served with prior written notice of a breach, and then later intentionally commit a similar breach. 
            * Such notice must specify the acts and omissions constituting the current breach, reference the prior breach, and state that the tenancy will end in not less than 30 days. (Va. Code Ann. § 55.1-1245E (2020)).

* **No notice necessary:** 
    * The landlord is not required to give the tenant any kind of notice if the tenant commits a criminal or willful act that is not remediable and poses a threat to health or safety. 
        * This includes any illegal drug activity. 
        * The landlord can go straight to court and file an eviction lawsuit against the tenant. (Va. Code Ann. § 55.1-1245C(2020)).

## Notice Without Cause
> A landlord cannot evict a tenant simply because the landlord does not like that person. If the landlord does not have a legal reason for eviction, then the landlord must wait until the term of the tenancy has expired before expecting the tenant to move. Depending on the type of tenancy, the landlord may still need to provide the tenant with notice.

* **Month-to-Month Tenancy**
    * To [end a month-to-month tenancy in Virginia](https://www.nolo.com/legal-encyclopedia/virginia-notice-requirements-terminate-month-month-tenancy.html), the landlord must give the tenant a 30-day notice informing the tenant that the tenancy will end at the end of the 30-day time frame. 
        * If the tenant has not moved out of the rental unit by the end of the 30 days, then the landlord can file an eviction lawsuit against the tenant. (Va. Code Ann. § 55.1-1253A (2020)).
* **Fixed-Term Lease**
    * If the tenant has a fixed-term lease or rental agreement, such as for one year, the landlord must wait until the end of the term before expecting the tenant to move. 
    * The landlord does not need to give the tenant notice to move unless the lease specifically requires it.

## Tenant Defenses
> Even though a landlord might have a valid legal cause to evict a tenant, the tenant can still decide to fight the eviction. The tenant might have a valid defense against the eviction, such as the [landlord discriminating](https://www.nolo.com/legal-encyclopedia/free-books/renters-rights-book/chapter5-2.html) against the tenant or the landlord failing to follow proper eviction procedures. By fighting the eviction, the tenant could delay the eviction and remain in the rental unit for longer. [Tenant Defenses to Evictions in Virginia](https://www.nolo.com/legal-encyclopedia/tenant-defenses-evictions-virginia.html) has more information.

## Removal of Tenant
> The only legal way for a landlord to remove a tenant is by winning an eviction lawsuit. Even then, the [landlord is not authorized to remove the tenant](https://www.nolo.com/legal-encyclopedia/lock-out-tenant-illegal-29799.html) from the rental unit; only a law enforcement officer can do that. It is illegal for a landlord in Virginia to attempt to force a tenant to move out of a rental unit, and the [tenant can sue the landlord for an illegal eviction](https://www.nolo.com/legal-encyclopedia/consequences-of-illegal-evictions.html).
> If the [tenant leaves property](https://www.nolo.com/legal-encyclopedia/handling-tenants-abandoned-property-an-overview.html) on the premises, the manner in which the landlord can dispose of the property depends on the circumstances:
* **Tenancy terminated for cause.** When giving a termination notice to the tenant (such as a five-day notice to pay rent or quit), the landlord can include in the notice a statement that any property left on the premises by the tenant after the tenant moves out of the rental unit will be considered abandoned after 24 hours. 
    * The landlord can then dispose of the property at the end of the 24 hours after the tenant has moved out. (Va. Code Ann. § 55.1-1254 (2020).)
* **Tenant abandoned rental.** If the lease or rental agreement requires tenant to notify landlord of an absence from the rental longer than seven days, and the tenant fails to give such notice, the landlord must give the tenant written notice that unless the tenant lets landlord know the rental isn’t abandoned within seven days, the landlord will assume the rental is abandoned. 
    * After the seven days is up, the landlord can dispose of tenant’s property if the tenant hasn’t responded. (Va. Code Ann. §§ 55.1-1254, 55.1-1249 (2020).)
* **All other situations.** If the landlord did not include the above statement in the termination notice, or give notice after the tenant abandoned the rental, then the landlord must give the tenant a written notice stating that the tenant has ten days to claim the property or the landlord will dispose of it within the 24 hours following the expiration of the ten days. (Va. Code Ann. § 55.1-1254 (2020)).



